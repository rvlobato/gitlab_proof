# Proof

[Verifying my OpenPGP key: openpgp4fpr:0EDF0C53F7399B5F254EB3F94B3854817E8BF5C5]


This is an OpenPGP proof that connects my OpenPGP key to this GitLab account. For details check out https://docs.keyoxide.org/advanced/openpgp-proofs/
